# imports here
import psycopg2
import csv
import os
from datetime import datetime

"""
Many of the attributes in the .csv files are reduntant or unnecessary or just completely empty.
After removing these, the ones we are left with give us the following tables.
It should be noted that every Plannungsraum is in exactly one district as evidenced by the fact 
that the IOR_Plannungsräume table contains exactly one entry for every Plannungsraum.

Ior_Plannungsräume: (plrID, plrName, Bezirksgrenzen-Gemeinde_Schlüssel, Größe_m^2 )
Bezirksgrenzen: (GemeindeName, Gemeinde_Schlüssel)
Fahrraddiebstahl: (Angelegt_am, Tatzeit_Anfang_Datum, Tatzeit_Anfang_Stunde, Tatzeit_Ende_Datum,
    Tatzeit_Ende_Stunde, Lor, Schadenshöhe, Versuch, Art_des_Fahrrads, Delikt, Erfassungsgrund)
"""

# We want to map all of the attribute names from the csv files to new names, which is what the
# following dictionaries are for (attributes not in the dictionaries will not be imported):

fahrraddiebstahl_attrs = { 
    # Names from the csv: 
    # ANGELEGT_AM,TATZEIT_ANFANG_DATUM,TATZEIT_ANFANG_STUNDE,TATZEIT_ENDE_DATUM,
    # TATZEIT_ENDE_STUNDE,LOR,SCHADENSHOEHE,VERSUCH,ART_DES_FAHRRADS,DELIKT,ERFASSUNGSGRUND

    "ANGELEGT_AM": "Angelegt_am", 
    "TATZEIT_ANFANG_DATUM": "Tatzeit_Anfang_Datum",
    "TATZEIT_ANFANG_STUNDE": "Tatzeit_Anfang_Stunde",
    "TATZEIT_ENDE_DATUM": "Tatzeit_Ende_Datum",
    "TATZEIT_ENDE_STUNDE": "Tatzeit_Ende_Stunde",
    "LOR": "Plannungsraum_ID",
    "SCHADENSHOEHE": "Schadenshoehe",
    "VERSUCH": "Versuch",
    "ART_DES_FAHRRADS": "Art_des_Fahrrads",
    "DELIKT": "Delikt",
    "ERFASSUNGSGRUND": "Erfassungsgrund"
}

bezirk_attrs = { 
    # Names from the csv: 
    # gml_id,Gemeinde_name,Gemeinde_schluessel,Land_name,Land_schluessel,Schluessel_gesamt

    "Gemeinde_name": "Name",
    "Gemeinde_schluessel": "ID",
}
plannungsraum_attrs = { 
    # Names from the csv: 
    # Name,description,timestamp,begin,end,altitudeMode,tessellate,extrude,visibility,drawOrder,
    # icon,PLR_ID,PLR_NAME,BEZ,STAND,GROESSE_M2

    "PLR_ID": "ID",
    "PLR_NAME": "Name",
    "BEZ": "Bezirk_ID",
    "GROESSE_M2": "Groesze"
}

types = { # anything not contained in here is a string which is most of the attributes.
    "Angelegt_am": "date", 
    "Tatzeit_Anfang_Datum": "date",
    "Tatzeit_Anfang_Stunde": "integer",
    "Tatzeit_Ende_Datum": "date",
    "Tatzeit_Ende_Stunde": "integer",
    "Plannungsraum_ID": "integer",
    "Schadenshoehe": "integer",
    "Versuch": "bool",
    "ID": "integer",
    "Bezirk_ID": "integer",
    "Groesze": "real", 
}

bool_conv = (lambda s: s == "Ja")
date_conv = (lambda s: datetime.strptime(s, "%d.%m.%Y").date())
type_converters = {
    "Angelegt_am": date_conv, 
    "Tatzeit_Anfang_Datum": date_conv,
    "Tatzeit_Anfang_Stunde": int,
    "Tatzeit_Ende_Datum": date_conv,
    "Tatzeit_Ende_Stunde": int,
    "Plannungsraum_ID": int,
    "Schadenshoehe": int,
    "Versuch": bool_conv,
    "ID": int,
    "Bezirk_ID": int,
    "Groesze": float, 
}
def get_converter(attr:str)->callable:
    
    if attr in type_converters:
        return type_converters[attr] 
    else:
        return str

def with_dtype(attr, tr = None):
    if tr != None:
        attri = tr[attr]
    else:
        attri = attr
    if attri in types:
        return f"{attr} {types[attri]}"
    return f"{attr} text" # default

umlaut_table = str.maketrans({
    "ä" : "ae",
    "ö" : "oe",
    "ü" : "ue",
    "ß" : "sz",
})

if os.path.isfile("database.db"):
    initialized = 1

cn = psycopg2.connect(
    host="localhost", 
    dbname="postgres", 
    user="postgres", 
    password="1234", 
    port="5432"
)

cursor = cn.cursor()

def init_table_from_csv(file_name:str, table_name:str, db_attrs:dict): # create all columns and rename them accordingly
    
    sql_add_row = f"""INSERT INTO {table_name} VALUES ({','.join(['%s']*len(db_attrs))}); """

    with open(file_name,'r') as file:
        
        reader = csv.DictReader(file)
        attributes = [a for a in reader.fieldnames if a in db_attrs]

        sql_create_table = f"""CREATE TABLE IF NOT EXISTS {table_name} ({','.join([with_dtype(a,tr=db_attrs) for a in attributes])}); """

        cursor.execute(sql_create_table)

        for row in reader: # create the table
            c = [get_converter(db_attrs[x])(row[x].translate(umlaut_table)) for x in attributes]
            #print(c)
            cursor.execute(sql_add_row, c)

        for attr in attributes: # rename columns
            if attr.upper() != db_attrs[attr].upper(): # will throw error otherwise
                sql_rename_column = f"""ALTER TABLE {table_name} RENAME COLUMN {attr} TO {db_attrs[attr]};"""
                cursor.execute(sql_rename_column)

cursor.execute("DROP TABLE Fahrraddiebstahl, Bezirk, Plannungsraum;") # create from scratch
init_table_from_csv(os.path.join("resources", "Fahrraddiebstahl.csv"), "Fahrraddiebstahl", fahrraddiebstahl_attrs)
init_table_from_csv(os.path.join("resources", "bezirksgrenzen.csv"), "Bezirk", bezirk_attrs)
init_table_from_csv(os.path.join("resources", "lor_planungsraeume_2021.csv"), "Plannungsraum", plannungsraum_attrs)  
cn.commit()

# example usage:
if __name__ == "__main__":
    cursor.execute("SELECT * FROM Plannungsraum P WHERE P.Bezirk_ID < 2;")
    print(cursor.fetchone())
    cursor.execute("SELECT DISTINCT Art_des_Fahrrads FROM Fahrraddiebstahl")
    print(cursor.fetchall())
