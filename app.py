from flask import Flask, render_template, flash, request
import multiprocessing
import forms
import graph
from connection import cursor
import os

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = "Shhhhh don't tell anyone ahh!"
app.config['UPLOAD_FOLDER'] = "static"

@app.route("/", methods=['GET', 'POST'])
def page():
    form = forms.QueryForm(request.form)
    print(form.errors)

    img = None

    if request.method == 'POST':
        
        print("Got values: ", dict(request.form))

        query, query_format_vals = forms.gen_query(request.form)
        nbins = 100
        if request.form["nbins"] != "":
            nbins = int(request.form["nbins"])
        
        cursor.execute(query, query_format_vals)
        datapoints = [x[0] for x in cursor.fetchall()]

        img = os.path.join(app.config["UPLOAD_FOLDER"], "plot.png")
        process = multiprocessing.Process(
            target=graph.gen_graph_image, 
            args=(datapoints, img), 
            kwargs={"bins": nbins}
        )
        process.start()
        process.join()
    
    return render_template('form.html', form=form, image=img)
    
if __name__ == "__main__":
    app.run()