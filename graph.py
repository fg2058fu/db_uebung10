import matplotlib.pyplot as plt

def gen_graph_image(datapoints, path, bins=100):
    fig, ax = plt.subplots()
    fig.set_size_inches(12,9)
    ax.hist(datapoints, bins)
    
    intervals = (max(datapoints) - min(datapoints))/bins
    ax.set_xlabel(f"Datum in {intervals}-Intervallen")
    ax.set_ylabel("Diebstähle")
    ax.set_title("Fahrraddiebstähle im Zeitraum")

    fig.savefig(path)