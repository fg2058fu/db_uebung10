import wtforms
from datetime import datetime
from connection import cursor

def get_options(table, attribute): 
    # get a list representing the domain of the attribute
    cursor.execute(f"SELECT DISTINCT {attribute} FROM {table}")
    return ["Alle"] + sorted([row[0] for row in cursor.fetchall()])

biketypes = get_options("Fahrraddiebstahl", "Art_des_Fahrrads")
districts = get_options("Bezirk", "Name")
plrs = get_options("Plannungsraum", "Name")

date_conv = (lambda s: datetime.strptime(s, "%Y-%m-%d").date())

class QueryForm(wtforms.Form):
    
    # choose from a list of all possible values
    district = wtforms.SelectField(label="Bezirk", choices=districts)
    plr = wtforms.SelectField(label="Plannungsraum", choices=plrs)
    biketype = wtforms.SelectField(label="Fahrradtype", choices=biketypes)

    # two decimal numbers rempresenting the range
    damage_min = wtforms.DecimalField(label="Schadenshöhe Minimum", validators=(wtforms.validators.Optional(),))
    damage_max = wtforms.DecimalField(label="Schadenshöhe Maximum", validators=(wtforms.validators.Optional(),))

    # bool representing if the data should be shown in relation to the area of the district/plr
    attempts = wtforms.BooleanField(label="Versuche mitzählen", validators=(wtforms.validators.Optional(),))

    # two decimal numbers rempresenting the range
    start_date = wtforms.DateField(label="Zeitraumanfang", format='%d.%m.%Y', validators=(wtforms.validators.Optional(),))
    end_date = wtforms.DateField(label="Zeitraumende", format='%d.%m.%Y', validators=(wtforms.validators.Optional(),))
    
    # number if bins for visualization
    nbins = wtforms.IntegerField(label="Anzahl Histogram-Bins (default: 100)", validators=(wtforms.validators.Optional(),))

def gen_query(form:dict):
    """generates an SQL-query to access the data requested by the form"""

    query_selection = "SELECT Tatzeit_Anfang_Datum FROM Fahrraddiebstahl F"
    query_condition = []
    query_format_vals = []

    # restrict location and biketype
    if form["plr"] != "Alle":
        query_selection += ", Plannungsraum P"
        query_condition.append("P.ID = F.Plannungsraum_ID")

        query_condition.append("P.Name = %s")
        query_format_vals.append(form["plr"])

    elif form["district"] != "Alle":
        query_selection += ", Bezirk B, Plannungsraum P"
        query_condition.append("B.ID = P.Bezirk_ID")
        query_condition.append("P.ID = F.Plannungsraum_ID")
        
        query_condition.append("B.Name = %s")
        query_format_vals.append(form["district"])

    if form["biketype"] != "Alle":
        query_condition.append("F.Art_des_Fahrrads = %s")
        query_format_vals.append(form["biketype"])

    # restrict damage attribute
    if form["damage_min"] != "":
        query_condition.append("F.Schadenshoehe > %s")
        query_format_vals.append(form["damage_min"])

    if form["damage_max"] != "":
        query_condition.append("F.Schadenshoehe < %s")
        query_format_vals.append(form["damage_max"])

    # restrict date attribute
    if form["start_date"] != "":
        query_condition.append("F.Tatzeit_Anfang_Datum > %s")
        query_format_vals.append(date_conv(form["start_date"]))

    if form["end_date"] != "":
        query_condition.append("F.Tatzeit_Ende_Datum < %s")
        query_format_vals.append(date_conv(form["end_date"]))
    
    if "attempts" not in form.keys():
        query_condition.append("F.Versuch = %s")
        query_format_vals.append(False)
    elif form["attempts"] != "y":
        raise ValueError("unrecognized bool.")

    # construct the query
    if query_condition:
        conditions = " AND ".join(query_condition)
        query = " ".join([query_selection, "WHERE", conditions])
    else:
        query = query_selection

    # excecute the query to get the dates.
    return (query, query_format_vals)
    